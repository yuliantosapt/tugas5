<?php

namespace App\Http\Controllers;

use App\Models\Employe;
use App\Models\position;
use App\Models\company;
use Illuminate\Http\Request;
use App\Exports\TableExport;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home', ['data' => Employe::get()]);
    }
    public function cek()
    {
        return view('cek', ['data' => Employe::get()]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = position::get();
        $company = company::get();
        //  $merge = array_merge($position,$company);
        // dd($company);
        return view('form', ['position' => $position, 'company' => $company]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Employe::create([
            'nama' => $request->nama,
            'atasan_id' => $request->position,
            'company_id' => $request->company
        ]);
        return redirect('/employe');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employe = Employe::find($id);
        $position = position::get();
        $company = company::get();
        return view('form_edit', ['employe' => $employe, 'position' => $position, 'company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Employe::whereId($id)->update([
            'nama' => $request->nama,
            'atasan_id' => $request->position,
            'company_id' => $request->company
        ]);
        return redirect('/employe');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employe::destroy($id);
        return redirect('/employe');
    }
    public function export()
    {
        return Excel::download(new TableExport, 'employe.xls');
    }
    public function export_pdf()
    {
       
      $pkrd = Employe::all();
 
      $pdf = PDF::loadview('cek',['data'=> $pkrd]);
       return $pdf->download('exportdata.pdf');

    }
}
