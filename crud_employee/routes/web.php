<?php

use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\CompanyController;
use Illuminate\Support\Facades\Route;
use App\Models\Employe;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('/employe', EmployeeController::class);
Route::resource('/company', CompanyController::class);
Route::get('/export', [EmployeeController::class, 'export']);
Route::get('/export-pdf', [EmployeeController::class, 'export_pdf']);
Route::get('cek', [EmployeeController::class, 'cek']);
