<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Hello, world!</title>
    <style>
tbody{
    text-align: center;
}
table{
    margin: auto;
    width: 50%;
}
    </style>
</head>

<body>
    <div class="container">
    <table border="1" >
        <thead>
            <tr>
                <th>Id</th>
                <th>Nama</th>
                <th >Posisi</th>
                <th >Perusahaan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $employee)
            <tr>
                <td>{{ $employee->id }}</td>
                <td>{{ $employee->nama }}</td>
                <td>
                    {{$employee->position->nama}}
                </td>
                <td >
                    {{$employee->company->nama}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</body>

</html>