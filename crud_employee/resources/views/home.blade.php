<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="{{asset('fontawesome/css/all.css')}}" rel="stylesheet">
    <title>Hello, world!</title>
</head>

<body>
    <div class="container">
        <div class="text-center">
            <h2>
                Crud Employee</h2>
        </div>
        <a class="btn btn-primary" href="/employe/create" role="button">Tambah Data</a>
        <a class="btn btn-primary" href="export" role="button">Export Data Excel</a>
        <a class="btn btn-primary" href="export-pdf" role="button">Export Data Pdf</a>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Atasan</th>
                    <th scope="col">Company</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $key => $employee)
                <tr>
                    <th scope="row">{{$key+1}}.</th>
                    <td>
                        {{$employee->nama}}
                    </td>
                    <td>
                        {{$employee->position->nama}}
                    </td>
                    <td>
                        {{$employee->company->nama}}
                    </td>
                    <td>
                        <span>
                            <a href="/employe/{{$employee->id}}/edit"><i class="fas fa-edit text-success"></i></a>
                        </span>
                        <span>
                            <form action="{{ url('/employe', ['id' => $employee->id]) }}" method="post">
                                <button type="submit" class="btn button-transparent fas fa-trash text-danger"></button>
                                <input type="hidden" name="_method" value="delete" />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </i>
                            </form>
                        </span>
                    </td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>
</body>

</html>