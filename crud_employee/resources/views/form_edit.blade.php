<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>
    <div class="container mt-5">
        <form action="/employe/{{$employe->id}}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="exampleFormControlInput1">Nama</label>
                <input type="text" name="nama" class="form-control" value="{{$employe->nama}}">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Posisi</label>
                <select class="form-control" name="position" id="exampleFormControlSelect1">
                    <option value="{{$employe->position->id}}">{{$employe->position->nama}}</option>
                    @foreach($position as $key => $position)
                    <option value="{{$position->id}} ">{{$position->nama}}</option>
                    @endforeach
                </select>

            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Perusahaan</label>
                <select class="form-control" name="company" id="exampleFormControlSelect1">
                    <option value="{{$employe->company->id}}">{{$employe->company->nama}}</option>

                    @foreach($company as $key => $company)
                    <option value="{{$company->id}}">{{$company->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mt-2">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</body>

</html>